'use strict';

/**
 * Libraries
 */
var gulp        = require('gulp'),
    include     = require('gulp-file-include'),
    minimist    = require('minimist'),
    rename      = require('gulp-rename'),
    debug       = require('gulp-debug');
/**
 * Congifs
 */
var options = minimist(process.argv.slice(2));

/**
 * Tasks
 */
// Build Task
// $ gulp build --client clientName/filename.html
gulp.task('build', function() {
  return gulp.src(options.client])
    .pipe(include({
      prefix: '@@',
      basepath: '@root'
    }))
    .pipe(gulp.dest('./'));
});

/**
 * To DO
 */
// 1. Build Dest based on input
// 2. Create `generate` task to copy template files
// 3. Make src variable relative to clients director